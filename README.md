# About Sleep
Only a certain amount of players on the server need to be sleeping to change to day. \
Every time a user enters a bed it will post a configurable message. \
You can configure all in the **plugins/Sleep/config.yml** file.
## Requirements
Sleep currently supports **CraftBukkit**, **Spigot** and [Paper](papermc.io/) **(recommended)**. \
Other server implementations may work, but we don't recommend them as they may cause compatibility issues.
## Links
|Source|Artifacts|Stats|
|:---:|:---:|:---:|
|[Gitlab](https://gitlab.com/Commandcracker/sleep)|[Spigot](https://www.spigotmc.org/resources/sleep.79642)|[bStats](https://bstats.org/plugin/bukkit/Sleep/7776)|
| |[Bukkit](https://dev.bukkit.org/projects/sleep-menager)|
## Metrics collection
Sleep collects anonymous server statistics through bStats, an open-source Minecraft statistics service. \
\
[![bStats](https://bstats.org/signatures/bukkit/Sleep.svg)](https://bstats.org/plugin/bukkit/Sleep/7776) \
If you'd like to disable metrics collection via bStats, you can edit the **plugins/bStats/config.yml** file.