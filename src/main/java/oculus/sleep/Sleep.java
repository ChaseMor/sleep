package oculus.sleep;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Sleep extends JavaPlugin implements Listener {
    public List<Player> playersSleeping = new ArrayList();
    private int Percentage, Players;
    private String PercentageMessageEnter, PercentageMessageLeave, PlayersMessageEnter, PlayersMessageLeave;
    private boolean PercentageEnabled;
    private static Sleep intance = null;

    public void onEnable() {
        new MetricsLite(this, 7776);
        Sleep.intance = this;

        saveDefaultConfig();
        reloadConfig();

        PercentageEnabled = getConfig().getBoolean("PercentageEnabled");
        Percentage = getConfig().getInt("Percentage.Percentage");
        PercentageMessageEnter = getConfig().getString("Percentage.MessageEnter");
        PercentageMessageLeave = getConfig().getString("Percentage.MessageLeave");
        Players = getConfig().getInt("Players.Players");
        PlayersMessageEnter = getConfig().getString("Players.MessageEnter");
        PlayersMessageLeave = getConfig().getString("Players.MessageLeave");

        Bukkit.getPluginManager().registerEvents(this, this);
        if (getConfig().getBoolean("Updater.Enabled")) {
            Bukkit.getPluginManager().registerEvents(new Updater(), this);
        }
    }

    public static Sleep getInstance() { return Sleep.intance; }

    @Override
    public void onDisable() { Sleep.intance = null; }

    public static void pluginmessage(String msg) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.hasPermission("sleep.notify")) {
                Bukkit.getServer().getConsoleSender().sendMessage("§b[" + getInstance().getDescription().getPrefix() + "] " + msg);
                p.sendMessage("§b[" + getInstance().getDescription().getPrefix() + "] " + msg);
            }
        }
    }

    @EventHandler
    public void onBedEnter(PlayerBedEnterEvent e) {
        if (!e.isCancelled()) {

            Player p = e.getPlayer();
            this.playersSleeping.add(p);

            boolean setday = false;
            int sleeping = this.playersSleeping.size();
            int onlineplayers = Bukkit.getOnlinePlayers().size();

            if (this.PercentageEnabled) {
                float percentage = (float) sleeping / (float) onlineplayers * 100.0F;

                Bukkit.broadcastMessage(
                        PercentageMessageEnter
                                .replace("{Player}", p.getName())
                                .replace("{Percentage}", String.valueOf((int)percentage))
                                .replace("{NeededPercentage}", String.valueOf(this.Percentage))
                );

                if (percentage >= (float) this.Percentage) {
                    setday = true;
                }
            } else {
                Bukkit.broadcastMessage(
                        PlayersMessageEnter
                                .replace("{Player}", p.getName())
                                .replace("{Players}", String.valueOf(sleeping))
                                .replace("{NeededPlayers}", String.valueOf(this.Players))
                );
                if (onlineplayers < this.Players && onlineplayers == sleeping) {
                    setday = true;
                } else if (sleeping >= this.Players) {
                    setday = true;
                }
            }
            if (setday) {
                World world = Bukkit.getWorlds().get(0);

                world.setTime(0);
                world.setWeatherDuration(0);
                world.setThunderDuration(0);
                world.setThundering(false);

                this.playersSleeping.clear();
            }
        }
    }

    @EventHandler
    public void onBedLeave(PlayerBedLeaveEvent e) {
        this.playersSleeping.remove(e.getPlayer());

        if (Bukkit.getWorlds().get(0).getTime() >= 12600L) {

            int sleeping = this.playersSleeping.size();

            if (this.PercentageEnabled) {
                float percentage = (float) sleeping / (float) Bukkit.getOnlinePlayers().size() * 100.0F;

                Bukkit.broadcastMessage(
                        PercentageMessageLeave
                                .replace("{Player}", e.getPlayer().getName())
                                .replace("{Percentage}", String.valueOf((int)percentage))
                                .replace("{NeededPercentage}", String.valueOf(this.Percentage))
                );
            } else {
                Bukkit.broadcastMessage(
                        PlayersMessageLeave
                                .replace("{Player}", e.getPlayer().getName())
                                .replace("{Players}", String.valueOf(sleeping))
                                .replace("{NeededPlayers}", String.valueOf(this.Players))
                );
            }
        }
    }
}
